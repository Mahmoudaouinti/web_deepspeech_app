import os
import ffmpeg
import uuid
import time
import sys
from deepspeech import Model
import scipy.io.wavfile as wav
from flask import Flask, flash, request, redirect, url_for, make_response, jsonify
from werkzeug.utils import secure_filename

UPLOAD_DIRECTORY = '/tmp'
ALLOWED_EXTENSIONS = set(['wav', 'mp3', 'flac'])

app = Flask(__name__)
app.secret_key = 'super secret key'
app.config['UPLOAD_DIRECTORY'] = UPLOAD_DIRECTORY
transcription_in_progress = False
print(transcription_in_progress)

if os.path.isfile("models/deepspeech-0.7.0-models.pbmm"):
    print("Starting the DeepSpeech Engine ")
    ds = Model('models/deepspeech-0.7.0-models.pbmm')
    ds.enableExternalScorer('models/deepspeech-0.7.0-models.scorer')

else:
    sys.exit('No DeepSpeech Model found, please download one')

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # Check if the file has filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            # import pdb;pdb.set_trace();
            filename = secure_filename(file.filename)
            filepath = os.path.join(UPLOAD_DIRECTORY, filename)
            file.save(filepath)
            file_converted = convert_audiofile(filepath)
            os.remove(filepath)
            return redirect(url_for('transcribe',
                                    filename=file_converted))
    return '''
    <!doctype html>
    <center><title>Upload an Audio File</title>
    <body style="background-color:OldLace;">
    <h1 style="color:red;">Please Upload Audio File </h1>
    <style>
        .button {
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
                }
    .button1 {background-color: #4CAF50;} /* Green */
    .button2 {background-color: #008CBA;} /* Blue */
    </style>
    <form method=post enctype=multipart/form-data>
    <input class="button button1" type=file name=file><br><br>
    <input class="button button2" type=submit value=Upload><br><br>
    </form>
    <br>
    '''
@app.route('/results/<filename>')
def transcribe(filename):
    global transcription_in_progress
    if (transcription_in_progress):
        print(" There is another transcription in progress, waiting 10 seconds...")
        time.sleep(10)
        transcribe(filename)
    print("Starting transcription")
    transcription_in_progress = True
    fs, audio = wav.read(os.path.join(UPLOAD_DIRECTORY, filename))
    processed_data = ds.stt(audio)
    os.remove(os.path.join(UPLOAD_DIRECTORY, filename))
    transcription_in_progress = False
    return processed_data

def convert_audiofile(file):
    filename = str(uuid.uuid4()) + ".wav"
    fileLocation = os.path.join(UPLOAD_DIRECTORY, filename)
    # import pdb;pdb.set_trace()
    stream = ffmpeg.input(file)
    stream = ffmpeg.output(stream, fileLocation, acodec='pcm_s16le', ac=1, ar='16k')
    ffmpeg.run(stream)
    return filename

@app.route('/api/v1/process', methods=['POST'])
def api_transcribe():
    if 'file' not in request.files:
        return make_response(jsonify({'error': 'No file part'}), 400)
    file = request.files['file']
    # if the request has a empty filename
    if file.filename == '':
        return make_response(jsonify({'error': 'No file in file parameter'}), 400)
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        filepath = os.path.join(UPLOAD_DIRECTORY, filename)
        file.save(filepath)
        file_converted = convert_audiofile(filepath)
        os.remove(filepath)
        return jsonify({'message': transcribe(filename=file_converted)})
    return make_response(jsonify({'error': 'Something went wrong :c'}), 400)

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

if __name__ == '__main__':

    app.debug = True
    app.run()

